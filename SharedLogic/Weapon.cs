﻿namespace Core.Controllers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using Core.Controllers;
    using Core.Data;
    using Core.Enum;

    public class Weapon
    {
        private const int BombDemage = 20;
        private const int CannonDemage = 40;
        private const int BombRange = 15;
        private const int CannonRange = 5;
        private int demage;
        private int range;

        public int Range
        {
            get
            {
                return range;
            }
        }

        public Weapon(ChooseWeapon weapon)
        {
            switch (weapon)
            {
                case ChooseWeapon.Bomb:
                    demage = BombDemage;
                    range = BombRange;
                    break;

                case ChooseWeapon.Cannon:
                    demage = CannonDemage;
                    range = CannonRange;
                    break;
                default:
                    break;
            }
        }

        public static ArrayList ShootXY(int Angle, int Strenght, Player player, List<Player> players, int ResolutionX, int ResolutionY)
        {
            ArrayList Points = new ArrayList();
            Points = ShootXYCommon(Angle, Strenght, player, players, ResolutionX, ResolutionY);
            return Points;
        }

        public static int ShootXY(int Angle, int Strenght, Player player, List<Player> players, int ResolutionX, int ResolutionY, List<Image> ImageList)
        {
            ArrayList Points = new ArrayList();
            Points = ShootXYCommon(Angle, Strenght, player, players, ResolutionX, ResolutionY);
            BulletPathWeb(Points, ImageList);
            Point[] PointsArray = (Point[])Points.ToArray(typeof(Point));
            Point x = PointsArray.Last();
            int LastShot = x.X + player.playerPosition;
            return LastShot;
        }

        private static ArrayList ShootXYCommon(int Angle, int Strenght, Player player, List<Player> players, int ResolutionX, int ResolutionY)
        {
            double X = 0;
            double t = 0.1;
            double Y;
            double AngleRad;
            int ShootingPlayerRange;
            string ShootingPlayerName;
            int ShootingPlayerPosition;
            ArrayList Points = new ArrayList();
            ShootingPlayerRange = player.range;
            ShootingPlayerName = player.Name;
            ShootingPlayerPosition = player.PlayerPosition;
            AngleRad = Angle * Math.PI / 180;
            player.ShotsCount++;

            do
            {
                X = Strenght * Math.Cos(AngleRad) * t;
                Y = (Strenght * Math.Sin(AngleRad) * t) - (5 * t * t);
                if (Convert.ToInt32(Y) >= 0 && Convert.ToInt32(Y) <= ResolutionY - 1 && ShootingPlayerPosition + Convert.ToInt32(X) - 1 > 0 && ShootingPlayerPosition + Convert.ToInt32(X) < ResolutionX)
                {
                    Points.Add(new Point(ResolutionX - (ShootingPlayerPosition + Convert.ToInt32(X)), ResolutionY - Convert.ToInt32(Y)));
                }

                t = t + 0.2;
            } while (Y >= 0 && (X + ShootingPlayerPosition >= 0 || X + ShootingPlayerPosition <= ResolutionX));
            int temp = 0;
            foreach (Player playerN in players)
            {
                if ((playerN.PlayerPosition - (X + ShootingPlayerPosition) < ShootingPlayerRange && playerN.PlayerPosition - (X + ShootingPlayerPosition) > -ShootingPlayerRange && ShootingPlayerPosition < ResolutionX / 2) || (playerN.PlayerPosition - (ShootingPlayerPosition - X) < ShootingPlayerRange && playerN.PlayerPosition - (ShootingPlayerPosition - X) > -ShootingPlayerRange && ShootingPlayerPosition > ResolutionX / 2))
                {
                    playerN.IsAlive = false;
                    player.Kills++;
                    return Points;
                }

                temp++;
            }

            return Points;
        }

        private static void BulletPathWeb(ArrayList Points, List<Image> ImageList)
        {
                using (var imgsave = new ImageSave())
                {
                    imgsave.ShowBulletPath(Points, ImageList);
                }
        }
    }
}
