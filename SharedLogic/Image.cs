﻿// Creates object for image with name and path property
using System.IO;

namespace Core.Controllers
{
    public class Image
    {
        public string ImageFile;
        public string ImageName;

        public Image(string imageName)
        {
            ImageName = imageName;
            ImageFile = Path.GetTempFileName().Replace(".tmp", ".png");
        } 
    }
}
