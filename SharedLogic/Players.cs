﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Controllers;
using Core.Enum;

namespace Core.Data
{
    public class Players
    {
         public bool Init = false;
         public int shotsCount = 0;
         public string PlayerOneNamea;
         public string PlayerTwoNamea;
         public int PlayerOnePositiona;
         public int PlayerTwoPositiona;
         public int LastShot;
         public int NumberOfPlayers;
         public int CurrentPlayer =0;
         public const int resolutionX = 800;
         public const int resolutionY = 445;
         public DateTime GameTime;

        public int ResolutionX
        {
            get
            {
                return resolutionX;
            }
        }

        public int ResolutionY
        {
            get
            {
                return resolutionY;
            }
        }

        public int ShotsCount
        {
            get
            {
                return shotsCount;
            }
            set
            {
                shotsCount = value;
            }
        }


    }


}
