﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Collections;
using Core.Data;


namespace Core.Controllers
{
    public class ImageSave : IDisposable
    {
        // Pointer to an external unmanaged resource.
        private IntPtr handle;
        // Other managed resource this class uses.
        //private Component component = new Component();
        // Track whether Dispose has been called.
        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            // any other managed resource cleanups you can do here
            GC.SuppressFinalize(this);
        }

        ~ImageSave()      // finalizer
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    //component.Dispose();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.
                CloseHandle(handle);
                handle = IntPtr.Zero;

                // Note disposing has been done.
                disposed = true;
            }
        }
        // Use interop to call the method necessary
        // to clean up the unmanaged resource.
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);

        // Use C# destructor syntax for finalization code.
        // This destructor will run only if the Dispose method
        // does not get called.
        // It gives your base class the opportunity to finalize.
        // Do not provide destructors in types derived from this class.

        public void AddPlayer(int PlayerPosition, string PlayerName, List<Image> ImageList)
        {
            Image ActualMapObj = ImageList.Find(x => x.ImageName.Contains("ActualMap"));
            var GameActualMap = Bitmap.FromFile(ActualMapObj.ImageFile);
            var ImageOne = Bitmap.FromFile(Path.GetFullPath(@"wwwroot\images\cavemanP1.png"));
            var GM = Graphics.FromImage(GameActualMap);
            Players players = new Players();
            GM.DrawImage(ImageOne, new Point(players.ResolutionX - PlayerPosition - 20, 390));
            GM.DrawString(PlayerName, new Font("Arial", 16), new SolidBrush(Color.Black), players.ResolutionX - PlayerPosition - 20, 380);
            Image MapTemp = ImageList.Find(x => x.ImageName.Contains("MapTemp"));
            GameActualMap.Save(MapTemp.ImageFile);
            GM.Dispose();
            GameActualMap.Dispose();
            GameActualMap = null;
        }

        public void SaveMap(List<Image> ImageList)
        {
            Image MapTempObj = ImageList.Find(x => x.ImageName.Contains("MapTemp"));
            var MapTemp = Bitmap.FromFile(MapTempObj.ImageFile);
            Image ActualMapObj = ImageList.Find(x => x.ImageName.Contains("ActualMap"));
            MapTemp.Save(ActualMapObj.ImageFile);
            MapTemp.Dispose();
            MapTemp = null;
        }

        public void MapIni(List<Image> ImageList)
        {
            Image ActualMapObj = ImageList.Find(x => x.ImageName.Contains("ActualMap"));
            var GameMap = Bitmap.FromFile(Path.GetFullPath(@"wwwroot\images\GameMap.png"));
            GameMap.Save(ActualMapObj.ImageFile);
            GameMap.Dispose();
            GameMap = null;
        }

        public void SaveMapToDisplay(List<Image> ImageList)
        {
            Image ActualMapObj = ImageList.Find(x => x.ImageName.Contains("ActualMap"));
            Image ActualMapShootObj = ImageList.Find(x => x.ImageName.Contains("ActualMapShoot"));
            var MapTemp = Bitmap.FromFile(ActualMapObj.ImageFile);
            MapTemp.Save(ActualMapShootObj.ImageFile);
            MapTemp.Dispose();
            MapTemp = null;
        }

        public void ShowBulletPath(ArrayList Points, List<Image> ImageList)
        {
            Pen redPen = new Pen(Color.Red, 3);
            Image ActualMapObj = ImageList.Find(x => x.ImageName.Contains("ActualMap"));
            var ShootMap = Bitmap.FromFile(ActualMapObj.ImageFile);
            var AM = Graphics.FromImage(ShootMap);
            Point[] PointsArray = (Point[])Points.ToArray(typeof(Point));
            AM.DrawCurve(redPen, PointsArray);
                var ActuaMapShoot = ImageList.Find(x => x.ImageName.Contains("ActualMapShoot"));
                File.Delete(ActuaMapShoot.ImageFile);
            ShootMap.Save(ActuaMapShoot.ImageFile);
            System.IO.File.Copy(ActualMapObj.ImageFile, Path.Combine(Path.GetFullPath(@"wwwroot\images\temp\"), Path.GetFileName(ActualMapObj.ImageFile)), true);
        }
    }
}
