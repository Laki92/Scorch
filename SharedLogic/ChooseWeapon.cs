﻿// Choose different types of weapons for players

namespace Core.Enum

{
    public enum ChooseWeapon
    {
        Bomb,
        Cannon
    }
}
