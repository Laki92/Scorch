﻿using System;
using Core.Enum;
namespace Core.Controllers


{
    public class Player
    {
        ////private const int STARTING_HEALTH = 100;
        public string Name;
        private bool isAlive = true;
        ////static private int health;
        private Random rnd = new Random();
        public int playerPosition;
        public int range;
        public int Kills;
        public int ShotsCount;

        public int PlayerPosition
        {
            get
            {
                return playerPosition;
            }
        }

        public bool IsAlive
        {
            get
            {
                return isAlive;
            }
            set
            {
                isAlive = value;
            }
        }

        public Weapon weaponPlayer;

        public Player(string name, ChooseWeapon weapon, int position)
        {
            Name = name;
            playerPosition = position;

            switch (weapon)
            {
                case ChooseWeapon.Bomb:
                    weaponPlayer = new Weapon(weapon);
                    range = weaponPlayer.Range;
                    break;
                case ChooseWeapon.Cannon:
                    weaponPlayer = new Weapon(weapon);
                    range = weaponPlayer.Range;
                    break;
                default:
                    break;
            }
        }


    }
}
