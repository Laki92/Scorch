﻿using Core.Controllers;
using Core.Enum;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleGame
{
    class Program
    {
        private static int DeadPlayers;

        public static void Main()
        {
            Random rnd = new Random();
            string angleInput;
            string strenghtInput;
            string NumberOfPlayersString;
            int NumberOfPlayersInt;
            int angleToInt;
            int strenghtToInt;
            List<Player> PlayerList = new List<Player>();
            int CurrentPlayer = 0;
            Console.WriteLine("Podaj ilość graczy");
            NumberOfPlayersString = Console.ReadLine();
            Int32.TryParse(NumberOfPlayersString, out NumberOfPlayersInt);
            for(int i = 0 ; i < NumberOfPlayersInt ; i++)
            {
                Console.WriteLine("Podaj nazwe gracza");
                string PlayerName = Console.ReadLine();
                Player PlayerIni = new Player(PlayerName, ChooseWeapon.Cannon, rnd.Next(1, (DisplayArray.ResolutionX) - 1));
                PlayerList.Add(PlayerIni);
            }

            DisplayArray Display = new DisplayArray(PlayerList);
            Console.SetWindowSize(120, 50);
            do
            {
                Display.PrintArray();
                do
                {
                    Console.WriteLine("podaj kat strzalu " + PlayerList[CurrentPlayer].Name);
                    angleInput = Console.ReadLine();
                    Int32.TryParse(angleInput, out angleToInt);
                } while (angleToInt < 1 && angleToInt > 180);

                do
                {
                    Console.WriteLine("podaj siłę strzalu " + PlayerList[CurrentPlayer].Name);
                    strenghtInput = Console.ReadLine();
                    Int32.TryParse(strenghtInput, out strenghtToInt);
                } while (strenghtToInt < 1 && strenghtToInt > 100);
                Display.BulletPath(Weapon.ShootXY(angleToInt, strenghtToInt, PlayerList[CurrentPlayer], PlayerList, DisplayArray.ResolutionX, DisplayArray.ResolutionY), Display);
                do
                {
                    CurrentPlayer++;
                } while (PlayerList[CurrentPlayer].IsAlive == false);
                if (CurrentPlayer >= NumberOfPlayersInt)
                {
                    CurrentPlayer = 0;
                }  

                if(DeadPlayers < NumberOfPlayersInt)
                {
                    Console.WriteLine(" Teraz kolej gracza " + PlayerList[CurrentPlayer].Name);
                    Console.WriteLine("Wciśnij dowolny klawisz by kontynuować");
                    Console.ReadKey();
                }

                foreach (Player player in PlayerList)
                {
                    if (player.IsAlive == false)
                    {
                        DeadPlayers++;
                    }
                }
                
                Display.CreateEmptyArray(PlayerList);
            } while (DeadPlayers < NumberOfPlayersInt - 1);
            Player last = PlayerList.First(a => a.IsAlive == true);
            Console.WriteLine("Wygrał gracz " + last.Name);
            Console.WriteLine("Wciśnij dowolny klawisz by kontynuować");
            Console.ReadKey();
        }
    }
}