﻿using Core.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Threading;

namespace ConsoleGame
{
    class DisplayArray
    {
        public const int ResolutionX = 100;
        public const int ResolutionY = 50;
        public static string[,] displayArray = new string[ResolutionX, ResolutionY];

        public DisplayArray(List<Player> players)
        {
            CreateEmptyArray(players);
        }

        public void CreateEmptyArray(List<Player> players)
        {
            for (int i = 0; i < ResolutionY; i++)
            {
                for (int j = 0; j < ResolutionX; j++)
                {
                    displayArray[j, i] = " ";
                }
            }

            foreach (Player player in players)
            {
                    displayArray[player.PlayerPosition, 0] = "x";
                    Console.WriteLine(player.Name + " na pozycji " + player.PlayerPosition);
                    Thread.Sleep(2000);
            }

        }

        public void BulletPath(ArrayList Points, DisplayArray display)
        {
            Point[] PointsArray = (Point[])Points.ToArray(typeof(Point));
            int x = PointsArray[0].X;
            for(int i = 0; i < Points.Count; i++)
            {
                displayArray[ResolutionX - PointsArray[i].X, ResolutionY - PointsArray[i].Y] = "o";
                display.PrintArray();
                Thread.Sleep(50);
            }
        }

        public void PrintArray()
        {
            Console.Clear();
            for (int i = ResolutionY - 1; i >= 0; i--)
            {
                for (int j = ResolutionX - 1; j >= 0; j--)
                {
                    Console.Write(displayArray[j, i]);
                }
                Console.WriteLine();
            }
        }
    }
}