﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Game
    {
        public string PlayerOneName { get; set; }
        public string PlayerTwoName { get; set; }
        public int Angle { get; set; }
        public int Strength { get; set; }
        public int PlayerOnePosition { get; set; }
        public int PlayerTwoPosition { get; set; }
        public bool ALive { get; set; }
        public int LastShot { get; set; }
        public int NumberOfPlayers { get; set; }
        public string ImageDirectory { get; set; }
    }
}
