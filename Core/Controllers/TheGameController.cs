﻿namespace Core.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Core.Data;
    using Core.Entities;
    using Core.Enum;
    using Core.Models;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    public class TheGameController : Controller
    {
        private readonly CoreContext _context;

        public TheGameController(CoreContext context)
        {
            _context = context;
        }

        private Random rnd = new Random();

        // First view for user
        public ActionResult Index()
        {
            List<Player> PlayerList = new List<Player>();
            Players players = new Players();
            players.ShotsCount = 0;
            HttpContext.Session.SetObject("PlayerList", PlayerList);
            HttpContext.Session.SetObject("CommonData", players);
            return View();
        }

        // Adds new player to the game
        public ActionResult AddPlayer(Game game)
        {
            List<Player> PlayerList = HttpContext.Session.GetObject<List<Player>>("PlayerList");
            Players players = HttpContext.Session.GetObject<Players>("CommonData");
            Player PlayerIni = new Player(game.PlayerOneName, ChooseWeapon.Bomb, rnd.Next(1, (players.ResolutionX) - 1));
            PlayerList.Add(PlayerIni);
            HttpContext.Session.SetObject("PlayerList", PlayerList);
            game.NumberOfPlayers = PlayerList.Count;
            return View(game);
        }

        // handles game mechanism and sends data to display
        public ActionResult Start(Game game)
        {
            List<Player> PlayerList = HttpContext.Session.GetObject<List<Player>>("PlayerList");
            List<Image> ImageListDisplay = HttpContext.Session.GetObject<List<Image>>("ImageList");
            Players players = HttpContext.Session.GetObject<Players>("CommonData");
            System.IO.DirectoryInfo di = new DirectoryInfo(@"wwwroot\images\temp\");
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            if (players.Init == true)
            {
                game.PlayerOnePosition = PlayerList[0].PlayerPosition;
                game.PlayerTwoPosition = PlayerList[1].PlayerPosition;
                game.PlayerOneName = PlayerList[0].Name;
                game.PlayerTwoName = PlayerList[1].Name;
                int DeadPlayers = 0;
                game.LastShot = Weapon.ShootXY(game.Angle, game.Strength, PlayerList[players.CurrentPlayer], PlayerList, players.ResolutionX, players.ResolutionY, ImageListDisplay);
                do
                {
                    players.CurrentPlayer++;
                    if (players.CurrentPlayer >= players.NumberOfPlayers)
                    {
                        players.CurrentPlayer = 0;
                    }
                } while (PlayerList[players.CurrentPlayer].IsAlive == false);
                game.PlayerTwoName = PlayerList[players.CurrentPlayer].Name;
                foreach (Player player in PlayerList)
                {
                    if (player.IsAlive == false)
                    {
                        DeadPlayers++;
                    }
                }

                if (DeadPlayers >= players.NumberOfPlayers - 1)
                {
                    Player last = PlayerList.First(a => a.IsAlive == true);
                    SaveResult(game.PlayerOneName, game.PlayerTwoName, last.Name);
                    game.PlayerOneName = last.Name;
                    return View("VictoryP1", game);
                }

                Image ActualMapShootObj = ImageListDisplay.Find(x => x.ImageName.Contains("ActualMapShoot"));
                System.IO.File.Copy(ActualMapShootObj.ImageFile, Path.Combine(Path.GetFullPath(@"wwwroot\images\temp\"), Path.GetFileName(ActualMapShootObj.ImageFile)));
                game.ImageDirectory = Path.Combine("/images/temp/", Path.GetFileName(ActualMapShootObj.ImageFile));
                HttpContext.Session.SetObject("PlayerList", PlayerList);
            }

            // Init game data
            if (players.Init == false)
            {
                List<Image> ImageList = new List<Image>();
                Image ActualMap = new Image("ActualMap");
                Image MapTemp = new Image("MapTemp");
                Image ActualMapShoot = new Image("ActualMapShoot");
                ImageList.Add(ActualMap);
                ImageList.Add(MapTemp);
                ImageList.Add(ActualMapShoot);
                HttpContext.Session.SetObject("ImageList", ImageList);
                players.NumberOfPlayers = PlayerList.Count;
                ImageController.Image(game.PlayerOnePosition, game.PlayerTwoPosition, PlayerList, ImageList);
                game.PlayerTwoName = PlayerList[players.CurrentPlayer].Name;
                Image ActualMapShootObj = ImageList.Find(x => x.ImageName.Contains("ActualMapShoot"));
                System.IO.File.Copy(ActualMapShootObj.ImageFile, Path.Combine(Path.GetFullPath(@"wwwroot\images\temp\"), Path.GetFileName(ActualMapShootObj.ImageFile)));
                game.ImageDirectory = Path.Combine("/images/temp/", Path.GetFileName(ActualMapShootObj.ImageFile));
                players.Init = true;
                players.GameTime = DateTime.Now;
            }

            HttpContext.Session.SetObject("CommonData", players);
            return View(game);
        }

        // GET: TheGame/Create
        public ActionResult Create()
        {
            return View();
        }

        public IActionResult Results()
        {
            var scores = _context.Scores
                .OrderBy(p => p.LocalDate)
                .ToList();
            return View(scores.ToList());
        }

        // POST: TheGame/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TheGame/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: TheGame/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TheGame/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TheGame/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        private void SaveResult(string playerOne, string playerTwo, string winner)
        {
            _context.Database.EnsureCreated();
            Players players = HttpContext.Session.GetObject<Players>("CommonData");
            List<Player> PlayerList = HttpContext.Session.GetObject<List<Player>>("PlayerList");
            foreach (Player player in PlayerList)
            {
                var score = new Score()
                {
                    PlayerName = player.Name,
                    NumberOfShots = players.ShotsCount,
                    Kills = player.Kills,
                    LocalDate = players.GameTime,
                };
                if(player.Name == winner)
                {
                    score.IsWinner = 1;
                }

                this._context.Scores.Add(score);
                this._context.SaveChanges();
            }
        }
    }
}