﻿// Creating map of the game
using System.Collections.Generic;
using System.IO;

namespace Core.Controllers
{
    public static class ImageController 
    {
        // Method for drawing image to display
        public static void Image(int PlayerOnePosition, int PlayerTwoPosition, List<Player> players, List<Image> ImageList)
        {
            Image ActualMapObj = ImageList.Find(x => x.ImageName.Contains("ActualMap"));
            Image MapTempObj = ImageList.Find(x => x.ImageName.Contains("MapTemp"));
            try
            {
                File.Delete(MapTempObj.ImageFile);
                File.Delete(ActualMapObj.ImageFile);
            }
            catch (IOException)
            {
               // return;
            }

            using (var imgsave = new ImageSave())
            {
                imgsave.MapIni(ImageList);
            }

            foreach (Player player in players)
            {
                using (var imgsave = new ImageSave())
                {
                    imgsave.AddPlayer(player.PlayerPosition, player.Name, ImageList);
                }

                File.Delete(ActualMapObj.ImageFile);

                using (var imgsave = new ImageSave())
                {
                    imgsave.SaveMap(ImageList);
                }

                File.Delete(MapTempObj.ImageFile);
            }

            using (var imgsave = new ImageSave())
            {
                imgsave.SaveMapToDisplay(ImageList);
            }
        }
    }
}
