﻿using System.Linq;
using Core.Entities;

namespace Core.Data
{
    public class CoreSeeder
    {
        private readonly CoreContext _ctx;

        public CoreSeeder(CoreContext ctx)
        {
            this._ctx = ctx;
        }

        // Create a first element in database if empty
        public void Seed()
        {
            this._ctx.Database.EnsureCreated();

            if (!this._ctx.Scores.Any())
            {
                var score = new Score()
                {
                    PlayerName = "Unknown Player",
                    NumberOfShots = 0
                };
                this._ctx.Scores.Add(score);
                this._ctx.SaveChanges();
            }
        }
    }
}
