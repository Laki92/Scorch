﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Core.Data
{
    public class CoreContext : DbContext
    {
        public CoreContext(DbContextOptions<CoreContext> options) : base(options)
        {

        }
        public DbSet<Score> Scores { get; set; }
    }
}
