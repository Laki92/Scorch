﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Entities
{
    public class Score
    {
        [Key]
        public int Id { get; set; }
        public string PlayerName { get; set; }
        public int NumberOfShots { get; set; }
        public int IsWinner { get; set; }
        public int Kills { get; set; }
        public DateTime LocalDate { get; set; }
    }
}
