﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Core.Migrations
{
    public partial class InitialDbMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TheWinner",
                table: "Scores",
                newName: "PlayerName");

            migrationBuilder.AddColumn<int>(
                name: "GameNumber",
                table: "Scores",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IsWinner",
                table: "Scores",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GameNumber",
                table: "Scores");

            migrationBuilder.DropColumn(
                name: "IsWinner",
                table: "Scores");

            migrationBuilder.RenameColumn(
                name: "PlayerName",
                table: "Scores",
                newName: "TheWinner");
        }
    }
}
