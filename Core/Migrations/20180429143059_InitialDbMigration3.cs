﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Core.Migrations
{
    public partial class InitialDbMigration3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GameNumber",
                table: "Scores");

            migrationBuilder.AddColumn<DateTime>(
                name: "LocalDate",
                table: "Scores",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LocalDate",
                table: "Scores");

            migrationBuilder.AddColumn<int>(
                name: "GameNumber",
                table: "Scores",
                nullable: false,
                defaultValue: 0);
        }
    }
}
